variable "vpc_name" {
  type        = string
  description = "(Required) VPC name"
}


variable "aws_region" {
  type        = string
  description = "(Required) AWS region"
}


variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}


variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
}

variable "app_name" {
  description = "Application name"
}



variable "http_port" {
  description = "HTTP port"
}

variable "https_port" {
  description = "HTTPS port"
}


variable "app_env" {
  type        = string
  description = "(Required) application environment"
}


variable "secret_word" {
  type        = string
  description = "Secret word"
}


variable "ecs_td_compatibility" {
  type        = string
  description = "Docker host: FARGATE or EC2"
}


variable "ecs_launch_type" {
  type        = string
  description = "ECS type: FARGATE or EC2"
}


variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = null
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = null
}

variable "ecs_network_mode" {
  type        = string
  description = "ECS network mode"
}



variable "container_count" {
  description = "Number of desired containers"
}


# <ROUTE53

variable "aws_hosted_zone" {
  type        = string
  description = "AWS Hosted Zone"
  default     = null
}

variable "route53_endpoint_name" {
  type        = string
  description = "AWS Route 53 Endpoint name"
  default     = null
}

variable "route53_endpoint_type" {
  type        = string
  description = "AWS Route 53 Endpoint type"
  default     = null
}

variable "aws_alb_dns_name" {
  type        = string
  description = "AWS ALB Name"
  default     = null
}

variable "aws_alb_zone_id" {
  type        = string
  description = "AWS ALB Zone Id"
  default     = null
}

variable "route53_alias_evaluate_target_health" {
  type        = bool
  description = "AWS Route 53 Evaluate Target Health"
  default     = false
}


# ROUTE53>


# <Certificates

variable "certificate_common_name" {
  type        = string
  description = "Certificate common name"
  default     = null
}

variable "certificate_organization" {
  type        = string
  description = "Certificate organization"
  default     = null
}

variable "certificate_organizational_unit" {
  type        = string
  description = "Certificate organizational unit"
  default     = null
}

variable "certificate_country" {
  type        = string
  description = "Certificate country"
  default     = null
}


# Certificates>


#------------------------------------------------------------------------------
# <Auto Scaling
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# AWS ECS SERVICE AUTOSCALING
#------------------------------------------------------------------------------
variable "ecs_cluster_name" {
  description = "Name of the ECS cluster"
}

variable "ecs_service_name" {
  description = "Name of the ECS service"
}

variable "max_cpu_threshold" {
  description = "Threshold for max CPU usage"
  default     = "85"
  type        = string
}
variable "min_cpu_threshold" {
  description = "Threshold for min CPU usage"
  default     = "10"
  type        = string
}

variable "max_cpu_evaluation_period" {
  description = "The number of periods over which data is compared to the specified threshold for max cpu metric alarm"
  default     = "3"
  type        = string
}
variable "min_cpu_evaluation_period" {
  description = "The number of periods over which data is compared to the specified threshold for min cpu metric alarm"
  default     = "3"
  type        = string
}

variable "max_cpu_period" {
  description = "The period in seconds over which the specified statistic is applied for max cpu metric alarm"
  default     = "60"
  type        = string
}
variable "min_cpu_period" {
  description = "The period in seconds over which the specified statistic is applied for min cpu metric alarm"
  default     = "60"
  type        = string
}

variable "scale_target_max_capacity" {
  description = "The max capacity of the scalable target"
  default     = 5
  type        = number
}

variable "scale_target_min_capacity" {
  description = "The min capacity of the scalable target"
  default     = 1
  type        = number
}


#------------------------------------------------------------------------------
# Auto Scaling>
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
# <S3 Cloudfront
#------------------------------------------------------------------------------



variable "website_domain_main" {
  description = "Main website domain, e.g. cloudmaniac.net"
  type        = string
}

variable "website_domain_redirect" {
  description = "Secondary FQDN that will redirect to the main URL, e.g. www.cloudmaniac.net"
  default     = null
  type        = string
}

variable "s3_cloudfront_tags" {
  description = "Tags added to resources"
  default     = {}
  type        = map(string)
}



#------------------------------------------------------------------------------
# S3 Cloudfront>
#------------------------------------------------------------------------------
