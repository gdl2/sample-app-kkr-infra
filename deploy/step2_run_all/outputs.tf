output "ecr_repository_url" {
  description = "Docker image repository URL"
  value       = data.aws_ecr_repository.image.repository_url
}

output "vpc_id" {
  value = module.sample_app_kkr_vpc.vpc_id
}

output "vpc_arn" {
  value = module.sample_app_kkr_vpc.vpc_arn
}

output "vpc_cidr_block" {
  value = module.sample_app_kkr_vpc.vpc_cidr_block
}

output "az_names" {
  value = module.sample_app_kkr_vpc.az_names
}

# output "aws_cert" {
#   value = module.sample_app_kkr_certificate.aws_certificate
# }