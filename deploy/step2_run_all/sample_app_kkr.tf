data "aws_ecr_repository" "image" {
  name = var.app_name
}

module "sample_app_kkr_vpc" {
  source   = "../../modules/vpc"
  vpc_name = var.vpc_name
}

module "sample_app_kkr_subnet" {
  source         = "../../modules/subnet"
  az_count       = var.az_count
  vpc_cidr_block = module.sample_app_kkr_vpc.vpc_cidr_block
  az_names       = module.sample_app_kkr_vpc.az_names
  vpc_id         = module.sample_app_kkr_vpc.vpc_id
}


module "sample_app_kkr_gateway" {
  source                  = "../../modules/gateway"
  vpc_id                  = module.sample_app_kkr_vpc.vpc_id
  vpc_main_route_table_id = module.sample_app_kkr_vpc.vpc_main_route_table_id
  az_count                = var.az_count
  aws_public_subnet       = module.sample_app_kkr_subnet.aws_public_subnet
  aws_private_subnet      = module.sample_app_kkr_subnet.aws_private_subnet
}


module "sample_app_kkr_security" {
  source     = "../../modules/security"
  app_name   = var.app_name
  vpc_id     = module.sample_app_kkr_vpc.vpc_id
  app_port   = var.app_port
  http_port  = var.http_port
  https_port = var.https_port
}

module "sample_app_kkr_certificate" {
  source                          = "../../modules/certificate"
  certificate_common_name         = var.certificate_common_name
  certificate_organization        = var.certificate_organization
  certificate_country             = var.certificate_country
  certificate_organizational_unit = var.certificate_organizational_unit

}


# module "sample_app_kkr_ecr" {
#   source   = "../modules/ecr"
#   app_name = var.app_name
# }


module "sample_app_kkr_load_balancer" {
  source                = "../../modules/load_balancer"
  app_name              = var.app_name
  aws_security_group_lb = module.sample_app_kkr_security.aws_security_group_lb
  aws_public_subnet     = module.sample_app_kkr_subnet.aws_public_subnet
  aws_private_subnet    = module.sample_app_kkr_subnet.aws_private_subnet
  http_port             = var.http_port
  https_port            = var.https_port
  app_port              = var.app_port
  aws_certificate       = module.sample_app_kkr_certificate.aws_certificate
  vpc_id                = module.sample_app_kkr_vpc.vpc_id
}

module "sample_app_kkr_ecs" {
  source               = "../../modules/ecs"
  app_name             = var.app_name
  ecs_td_compatibility = var.ecs_td_compatibility
  iam_execution_role   = module.sample_app_kkr_security.iam_execution_role
  fargate_cpu          = var.fargate_cpu
  fargate_memory       = var.fargate_memory
  app_image            = "${data.aws_ecr_repository.image.repository_url}:latest"
  secret_word          = var.secret_word
  ecs_launch_type      = var.ecs_launch_type
  ecs_network_mode     = var.ecs_network_mode
  # aws_public_subnet       = module.sample_app_kkr_subnet.aws_public_subnet
  aws_private_subnet       = module.sample_app_kkr_subnet.aws_private_subnet
  app_port                 = var.app_port
  alb_listener_frontend    = module.sample_app_kkr_load_balancer.alb_listener_frontend
  aws_alb_target_group_app = module.sample_app_kkr_load_balancer.aws_alb_target_group_app
  aws_sg_ecs_tasks         = module.sample_app_kkr_security.aws_sg_ecs_tasks
  container_count          = var.container_count
}


module "sample_app_kkr_auto_scaling" {
  source                    = "../../modules/auto_scaling"
  app_name                  = var.app_name
  aws_region                = var.aws_region
  ecs_cluster_name          = var.ecs_cluster_name
  ecs_service_name          = var.ecs_service_name
  max_cpu_threshold         = var.max_cpu_threshold
  min_cpu_threshold         = var.min_cpu_threshold
  max_cpu_evaluation_period = var.max_cpu_evaluation_period
  min_cpu_evaluation_period = var.min_cpu_evaluation_period
  max_cpu_period            = var.max_cpu_period
  min_cpu_period            = var.min_cpu_period
  scale_target_max_capacity = var.scale_target_max_capacity
  scale_target_min_capacity = var.scale_target_min_capacity

}


module "sample_app_kkr_route53" {
  source                               = "../../modules/route53"
  aws_hosted_zone                      = var.aws_hosted_zone
  route53_endpoint_name                = var.route53_endpoint_name
  route53_endpoint_type                = var.route53_endpoint_type
  route53_alias_evaluate_target_health = var.route53_alias_evaluate_target_health
  aws_alb_dns_name                     = module.sample_app_kkr_load_balancer.aws_alb_dns_name
  aws_alb_zone_id                      = module.sample_app_kkr_load_balancer.aws_alb_zone_id
}

module "sample_app_kkr_s3_cloudfront" {
  source                  = "../../modules/s3_cloudfront_static_hosting"
  website_domain_name     = var.aws_hosted_zone
  s3_cloudfront_tags      = var.s3_cloudfront_tags
  website_domain_redirect = var.website_domain_redirect
  website_domain_main     = var.website_domain_main
  aws_region              = var.aws_region

}





terraform {
  backend "s3" {
    bucket         = "iac-cicd"
    key            = "sample-app-kkr/ecs/dev/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "iac-sample-app-kkr-terraform-state-lock"
  }
}