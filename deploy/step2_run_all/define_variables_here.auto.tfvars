vpc_name             = "sample-app-kkr-vpc"
aws_region           = "us-east-1"
az_count             = 2
app_name             = "sample-app-kkr"
app_port             = 3000
http_port            = 80
https_port           = 443
ecs_td_compatibility = "FARGATE"
fargate_memory       = 512
fargate_cpu          = 256
app_env              = "dev"
secret_word          = "MAY THE FORCE BE WITH YOU!!"
ecs_launch_type      = "FARGATE"
ecs_network_mode     = "awsvpc"
container_count      = 4

# <Route53
aws_hosted_zone                      = "unmeetings.org."
route53_endpoint_name                = "sampleappkkr.unmeetings.org"
route53_endpoint_type                = "A"
route53_alias_evaluate_target_health = false
# Route53>

# <Certificates
certificate_common_name         = "sampleappkkr.unmeetings.org"
certificate_country             = "US"
certificate_organization        = "sampleappkkr"
certificate_organizational_unit = "sampleappkkr"
# Certificates>


# <Auto Scaling
ecs_cluster_name          = "sample-app-kkr-ecs-cluster"
ecs_service_name          = "sample-app-kkr-ecs-service"
max_cpu_threshold         = "80"
min_cpu_threshold         = "15"
max_cpu_evaluation_period = "3"
min_cpu_evaluation_period = "3"
max_cpu_period            = "60"
min_cpu_period            = "180"
scale_target_max_capacity = "4"
scale_target_min_capacity = "1"
# Auto Scaling>

# <S3 Cloudfront
# website-domain-main="sampleappkkr.unmeetings.org"
# website-domain-redirect="www.sampleappkkr.unmeetings.org"

website_domain_main     = "sampleappkkrtestreports.unmeetings.org"
website_domain_redirect = "www.sampleappkkrtestreports.unmeetings.org"

s3_cloudfront_tags = {
  name        = "sampleappkkrtestreports"
  environment = "dev"
  project     = "devops"
service = "s3 cloudfront" }



