variable "aws_region" {
  type        = string
  description = "(Required) AWS region"
}
