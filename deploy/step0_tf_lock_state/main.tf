# create a DynamoDB table for locking the state file
resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name           = "iac-sample-app-kkr-terraform-state-lock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "DynamoDB Terraform State Lock Table"
    Project     = "devops"
    Environment = "dev"
    Service     = "dynamodb"
  }
}


terraform {
  backend "s3" {
    bucket  = "iac-cicd"
    key     = "sample-app-kkr/terraform-remote-state-lock-dynamodb/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}
