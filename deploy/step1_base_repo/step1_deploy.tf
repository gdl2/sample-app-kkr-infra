
module "sample_app_kkr_ecr" {
  source   = "../../modules/ecr"
  app_name = var.app_name
}

terraform {
  backend "s3" {
    bucket         = "iac-cicd"
    key            = "sample-app-kkr/ecr/dev/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "iac-sample-app-kkr-terraform-state-lock"
  }
}