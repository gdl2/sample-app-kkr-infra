output "ecr_repo_url" {
  description = "Docker image repository"
  value       = module.sample_app_kkr_ecr.ecr_repo_url
}
