
variable "app_name" {
  description = "Application name"
}

variable "aws_region" {
  type        = string
  description = "(Required) AWS region"
}
