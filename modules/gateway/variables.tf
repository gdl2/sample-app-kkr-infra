variable "vpc_id" {
    type = string
    description = "VPC ID"
}


variable "vpc_main_route_table_id" {
    type = string
    description = "VPC main routing table"
}


variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}

variable "aws_public_subnet" {
  description = "AWS public subnet"
}


variable "aws_private_subnet" {
  description = "AWS private subnet"
}


