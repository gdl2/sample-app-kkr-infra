

variable "iam_execution_role" {
    description = "IAM execution role for ECS"
}


variable "container_count" {
    description = "Number of desired containers"
}



variable "app_image" {
    description = "Docker image URL"
}

variable "ecs_td_compatibility" {
  type        = string
  description = "Docker host: FARGATE or EC2"
}

variable "ecs_network_mode" {
  type        = string
  description = "ECS network mode"
}



variable "ecs_launch_type" {
  type        = string
  description = "ECS type: FARGATE or EC2"
}


variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = null
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = null
}



variable "secret_word" {
  type        = string
  description = "Secret word"
}




# variable "vpc_id" {
#     type = string
#     description = "VPC ID"
# }


variable "app_name" {
    description = "Application name"
}


variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
}


variable "alb_listener_frontend" {
  description = "Load balancer listener for front end"
}


variable "aws_alb_target_group_app" {
  description = "ECS target group app"
}



variable "aws_sg_ecs_tasks" {
  description = "ECS tasks security group"
}





# variable "http_port" {
#     description = "HTTP port"
# }

# variable "https_port" {
#     description = "HTTPS port"
# }



# variable "aws_public_subnet" {
#   description = "AWS public subnet"
# }


variable "aws_private_subnet" {
  description = "AWS private subnet"
}


# variable "aws_security_group_lb" {
#   description = "Security group load balancer"
# }


# variable "aws_certificate" {
#   description = "AWS Certificate"
# }




