resource "aws_ecs_cluster" "main" {
  name = "${var.app_name}-ecs-cluster"
}

resource "aws_ecs_task_definition" "app" {
  family                   = var.app_name
  network_mode             = var.ecs_network_mode
  requires_compatibilities = [var.ecs_td_compatibility]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  execution_role_arn       = var.iam_execution_role.arn
  task_role_arn            = var.iam_execution_role.arn
  container_definitions = <<TASK_DEFINITION
[
    {
        "cpu": ${var.fargate_cpu},
        "labels": {
                "tags": "docker"
            },
        "environment": [
            {"name": "SECRET_WORD", "value": "${var.secret_word}"}
        ],
        "image": "${var.app_image}",
        "memory": ${var.fargate_memory},
        "name": "${var.app_name}",
        "portMappings": [
            {
                "containerPort": ${var.app_port},
                "hostPort": ${var.app_port}
            }
        ]
    }
]
TASK_DEFINITION
}

resource "aws_ecs_service" "main" {
  name            = "${var.app_name}-ecs-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.app.arn
  desired_count   = var.container_count
  launch_type     = var.ecs_launch_type

  network_configuration {
    security_groups = [var.aws_sg_ecs_tasks.id]
    subnets         = var.aws_private_subnet.*.id
  }

  load_balancer {
    target_group_arn = var.aws_alb_target_group_app.id
    container_name   = var.app_name
    container_port   = var.app_port
  }

  depends_on = [
    var.alb_listener_frontend,
  ]
}


