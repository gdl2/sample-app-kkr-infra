
output "aws_public_subnet" {
    value = aws_subnet.public
}

output "aws_private_subnet" {
    value = aws_subnet.private
}
