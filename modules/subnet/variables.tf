
variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = null
}


variable "vpc_id" {
    type = string
    description = "VPC ID"
}



variable "vpc_cidr_block" {
  description = "VPC CIDR block"
}


variable "az_names" {
    description = "Availability Zone names"
}

# variable "vpc_cidr_block_1" {
#   description = "Number of AZs to cover in a given AWS region"
#   default     = var.vpc_cidr_block
# }



