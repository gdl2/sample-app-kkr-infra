# Create var.az_count private subnets, each in a different AZ
resource "aws_subnet" "private" {
  count             = var.az_count
  cidr_block        = cidrsubnet(var.vpc_cidr_block, 8, count.index)
  availability_zone = var.az_names[count.index]
  vpc_id            = var.vpc_id
  tags = {
    Name = format("private-subnet-%02d", count.index+1 )
  }
}

# Create var.az_count public subnets, each in a different AZ
resource "aws_subnet" "public" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(var.vpc_cidr_block, 8, var.az_count + count.index)
  availability_zone       = var.az_names[count.index]
  vpc_id                  = var.vpc_id
  map_public_ip_on_launch = true
  tags = {
    Name = format("public-subnet-%02d", count.index+1 )
  }
}

