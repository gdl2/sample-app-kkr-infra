variable "aws_hosted_zone" {
  type        = string
  description = "AWS Hosted Zone"
  default     = null
}

variable "route53_endpoint_name" {
  type        = string
  description = "AWS Route 53 Endpoint name"
  default     = null
}

variable "route53_endpoint_type" {
  type        = string
  description = "AWS Route 53 Endpoint type"
  default     = null
}

variable "aws_alb_dns_name" {
  type        = string
  description = "AWS ALB Name"
  default     = null
}

variable "aws_alb_zone_id" {
  type        = string
  description = "AWS ALB Zone Id"
  default     = null
}

variable "route53_alias_evaluate_target_health" {
  type        = bool
  description = "AWS Route 53 Evaluate Target Health"
  default     = false
}