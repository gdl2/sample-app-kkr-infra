output "aws_hosted_zone_id" {
  description = "AWS Route53 Hosted Zone"
  value       = data.aws_route53_zone.hosted_zone.zone_id
}
