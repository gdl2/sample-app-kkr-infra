
data "aws_route53_zone" "hosted_zone" {
  name         = var.aws_hosted_zone
#   private_zone = true
}

resource "aws_route53_record" "dns_records" {
  zone_id = data.aws_route53_zone.hosted_zone.zone_id
  name    = var.route53_endpoint_name
  type    = var.route53_endpoint_type

  alias {
      name                   = var.aws_alb_dns_name
      zone_id                = var.aws_alb_zone_id
      evaluate_target_health = var.route53_alias_evaluate_target_health
  }
}