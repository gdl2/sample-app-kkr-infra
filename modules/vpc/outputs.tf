output "vpc_id" {
    value = aws_vpc.main.id
}

output "vpc_arn" {
    value = aws_vpc.main.arn
}

output "vpc_cidr_block" {
    value = aws_vpc.main.cidr_block
}

output "az_names" {
    value = data.aws_availability_zones.available.names
}


output "vpc_main_route_table_id" {
    value = aws_vpc.main.main_route_table_id
}