### Certificate
resource "tls_private_key" "tls" {
  algorithm = "RSA"
}


resource "tls_self_signed_cert" "tls" {
  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.tls.private_key_pem

  subject {
    common_name  = var.certificate_common_name
    organization = var.certificate_organization
    country = var.certificate_country
    organizational_unit=var.certificate_organizational_unit
  }

  validity_period_hours = 24

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}


resource "aws_acm_certificate" "cert" {
  private_key      = tls_private_key.tls.private_key_pem
  certificate_body = tls_self_signed_cert.tls.cert_pem
}


