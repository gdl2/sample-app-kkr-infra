output "private_key_pem" {
  description = "Certicate Private Key"
  value       = tls_private_key.tls.private_key_pem
  sensitive = true
}

output "public_key_pem" {
  description = "Certicate Public Key"
  value       = tls_private_key.tls.public_key_pem
  sensitive = true
}


output "cert_pem" {
  description = "Certicate PEM"
  value       = tls_self_signed_cert.tls.cert_pem
  sensitive = true
}



output "acm_arn" {
  description = "Certicate ARN"
  value       = aws_acm_certificate.cert.arn
}


output "acm_id" {
  description = "Certicate id"
  value       = aws_acm_certificate.cert.id
}

output "aws_certificate" {
  description = "Certicate"
  value       = aws_acm_certificate.cert
  sensitive = true
}
