variable "certificate_common_name" {
  type        = string
  description = "Certificate common name"
  default     = null
}

variable "certificate_organization" {
  type        = string
  description = "Certificate organization"
  default     = null
}

variable "certificate_organizational_unit" {
  type        = string
  description = "Certificate organizational unit"
  default     = null
}

variable "certificate_country" {
  type        = string
  description = "Certificate country"
  default     = null
}
