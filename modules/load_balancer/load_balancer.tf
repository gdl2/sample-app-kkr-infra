### ALB


resource "aws_alb" "main" {
  name            = "${var.app_name}-ecs-lb"
  subnets         = var.aws_public_subnet.*.id
  security_groups = [var.aws_security_group_lb.id]
}


resource "aws_alb_listener" "https" {
  load_balancer_arn = aws_alb.main.id
  port              = var.https_port
  protocol          = "HTTPS"
  certificate_arn   = var.aws_certificate.arn

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }
}



resource "aws_alb_target_group" "app" {
  name        = "${var.app_name}-ecs-tg"
  port        = var.http_port
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"
  deregistration_delay = 30
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.main.id
  port              = var.http_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "forward"
  }
}
