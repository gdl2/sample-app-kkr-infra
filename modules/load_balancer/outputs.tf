output "aws_cert" {
    value = var.aws_certificate
    sensitive = true
}

output "alb_listener_frontend" {
    value = aws_alb_listener.front_end
}


output "aws_alb_target_group_app" {
    value = aws_alb_target_group.app
}

output "aws_alb_dns_name" {
    value = aws_alb.main.dns_name
}

output "aws_alb_zone_id" {
    value = aws_alb.main.zone_id
}


output "aws_alb_name" {
    value = aws_alb.main.name
}
