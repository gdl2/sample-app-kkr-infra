output "aws_security_group_lb" {
    value = aws_security_group.lb
}

output "aws_sg_ecs_tasks" {
    value = aws_security_group.ecs_tasks
}



output "iam_execution_role" {
    value = aws_iam_role.ecs_task_execution_role
}

