variable "vpc_id" {
    type = string
    description = "VPC ID"
}


variable "app_name" {
    description = "Application name"
}


variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
}


variable "http_port" {
    description = "HTTP port"
}

variable "https_port" {
    description = "HTTPS port"
}
